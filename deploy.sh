#!/bin/bash

# new version
yarn version

# build production code
yarn build

# write out current version
node -e "console.log(require('./package.json').version)" > build/version

# copy code
rsync -avz --progress --delete --rsync-path="sudo rsync" ./build/ ettinsmore:/var/www/attend-ui/public_html/
