import delay from 'delay'

// Run a promise-returning function every x seconds.
// SetTimeout(0) ensures that we don't block the caller
const runningIntervals = new Set()

export const asyncInterval = (time, task) => {
  let intervalId = Math.random()
  runningIntervals.add(intervalId)

  setTimeout(async () => {
    let startTime = Date.now()
    while (true) {
      // check if it's been canceled
      if (!runningIntervals.has(intervalId)) {
        return
      }

      await task()
      const endTime = Date.now()
      const elapsed = endTime - startTime
      await delay(Math.max(0, time - elapsed))
      startTime = endTime
    }
  }, 0)

  return intervalId
}

export const cancelInterval = (id) => {
  runningIntervals.delete(id)
}
