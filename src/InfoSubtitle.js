import React from 'react'
import styled from 'styled-components'
import _ from 'lodash'
import dayjs from 'dayjs'

const InfoSubtitle = ({ currentClasses }) => {
  let classesText = 'no classes'
  if (currentClasses.length) {
    classesText = _.uniq(currentClasses.map((cls) => cls.classAbbrev)).join(
      ', ',
    )
  }

  return <Container>Recording attendance for: {classesText}.</Container>
}

export default InfoSubtitle

const Container = styled.div``
