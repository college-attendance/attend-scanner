import PouchDB from 'pouchdb'
import findPlugin from 'pouchdb-find'

// databases
const baseurl = process.env.REACT_APP_DB_URL

export const studentsDB = new PouchDB('students')
export const studentsRemoteDB = new PouchDB(baseurl + 'students')

export const classesDB = new PouchDB('classes')
export const classesRemoteDB = new PouchDB(baseurl + 'classes')

export const scansDB = new PouchDB('scans')
export const scansRemoteDB = new PouchDB(baseurl + 'scans')

// enable mango queries
PouchDB.plugin(findPlugin)
studentsDB.createIndex({
  index: { fields: ['badgeId'] },
})

// replication
studentsDB.replicate.from(studentsRemoteDB, { live: true })
classesDB.replicate.from(classesRemoteDB, { live: true })
scansDB.sync(scansRemoteDB, { live: true })

// subscription functions
const changeListeners = new Set()
export const dbSub = (fn) => changeListeners.add(fn)
export const dbUnsub = (fn) => changeListeners.delete(fn)
const subForChanges = (db) => {
  db.changes({ since: 'now', live: true }).on('change', (change) => {
    changeListeners.forEach((l) => l(change))
  })
}
;[studentsDB, classesDB, scansDB].forEach((db) => subForChanges(db))

// look up a student by their badge ID
export const studentFromId = async (id) => {
  // return {}

  const result = await studentsDB.find({
    selector: {
      badgeId: id,
    },
  })

  return result.docs[0]
}

// save a scan to the DB
export const writeScan = async (student, matchedClass) => {
  const scan = {
    _id: String(Date.now()),
    badgeId: student.badgeId,
    courseInstanceId: matchedClass.classInstanceId,
    firstName: student.firstName,
    lastName: student.lastName,
    userName: student.userName,
    personId: student.personId,
  }
  const result = await scansDB.put(scan)
}

// Grab the entire class listing
export const getAllClasses = async (id) => {
  const result = await classesDB.allDocs({ include_docs: true })
  const docs = result.rows.map((row) => row.doc)
  return docs
}

// debugging functions
window.allStudents = async () => {
  return await studentsDB.allDocs({ include_docs: true })
}
window.allClasses = async () => {
  return await getAllClasses()
}
window.allScans = async () => {
  return await scansDB.allDocs({ include_docs: true })
}
window.studentFromId = async (id) => {
  return await studentFromId(id)
}
