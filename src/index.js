import React from 'react'
import ReactDOM from 'react-dom'
import './normalize.css'
import * as serviceWorker from './serviceWorker'
import { createGlobalStyle } from 'styled-components'
import App from './App'
import { version } from '../package.json'
import { asyncInterval } from './util'

const GlobalStyle = createGlobalStyle`
  html {
    font-family: 'Open Sans', sans-serif;
  }
`

const domContainer = document.querySelector('#root')
ReactDOM.render(
  <React.Fragment>
    <GlobalStyle />
    <App />
  </React.Fragment>,
  domContainer,
)

// Register service worker
serviceWorker.register()

// are we running on localHost?
const isLocalhost = Boolean(
  window.location.hostname === 'localhost' ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === '[::1]' ||
    // 127.0.0.1/8 is considered localhost for IPv4.
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/,
    ),
)

// check for updates
if (!isLocalhost) {
  asyncInterval(1000, async () => {
    const serverVersion = await getVersion()
    if (serverVersion !== version) {
      console.log('reloading')
      // window.location.reload()
    }
  })
}

const getVersion = async () => {
  const response = await fetch('/version')
  return await response.text()
}
