import xml2js from 'xml2js'
import pMap from 'p-map'
import { format } from 'date-fns'
import dayjs from 'dayjs'
import { asyncInterval } from './util'
import { getLastPopuliUsersSync } from './db'

export const getClasses = async (config) => {
  let sessions = []

  await Promise.all(
    config.classes.map(async (instanceId) => {
      const classInfo = await getClassInfo(config, instanceId)
      const classes = await getClassMeetings(config, classInfo, instanceId)
      sessions = sessions.concat(classes)
    }),
  )
  return sessions
}

export const getClassInfo = async (config, instanceId) => {
  const response = (await callPopuli(config, {
    task: 'getCourseInstance',
    instanceID: instanceId,
  })).response

  return {
    name: response.name[0],
    abbreviation: response.abbrv[0],
  }
}

export const getClassMeetings = async (config, classInfo, instanceId) => {
  const response = await callPopuli(config, {
    task: 'getCourseInstanceMeetings',
    instanceID: instanceId,
  })

  return response.response.meeting.map((meeting) => {
    let meetingId
    if (meeting.meetingid) {
      meetingId = meeting.meetingid[0]
    }

    return {
      classInstanceId: instanceId,
      className: classInfo.name,
      classAbbrev: classInfo.abbreviation,
      counts_toward_attendance_hours: meeting.counts_toward_attendance_hours[0],
      counts_toward_clinical_hours: meeting.counts_toward_clinical_hours[0],
      end: dayjs(meeting.end[0]).valueOf(),
      start: dayjs(meeting.start[0]).valueOf(),
      roomId: meeting.roomid[0],
      meetingId,
    }
  })
}

const getClassInstance = async (config) => {
  const response = await callPopuli(config, { task: 'getCourseInstance' })
}

export const callPopuli = async (config, params) => {
  const formData = new FormData()

  const { accessKey, endpoint } = config

  // append API key
  formData.append('access_key', accessKey)

  // append custom params
  Object.keys(params).forEach((key) => {
    formData.append(key, params[key])
  })

  const response = await fetch(endpoint, {
    method: 'POST',
    body: formData,
  })

  if (response.error) {
    throw new Error(response.error)
  }

  const body = await response.text()

  return await parseXmlString(body)
}

const parseXmlString = (xmlString) => {
  return new Promise((resolve, reject) => {
    xml2js.parseString(xmlString, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}

const getUnix = (date) => {
  return (date.getTime() / 1000) | 0
}
