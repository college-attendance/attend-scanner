import React from 'react'
import styled from 'styled-components'
import io from 'socket.io-client'

import { asyncInterval, cancelInterval } from './util'
import ScanResults from './ScanResults'
import InfoSubtitle from './InfoSubtitle'
// import { scanId } from './scanner'
import { studentFromId, writeScan, dbSub, dbUnsub, getAllClasses } from './db'
// import { startSync } from './sync'
import * as populi from './populi'
import dayjs from 'dayjs'
<<<<<<< HEAD
import _ from 'lodash'
=======
>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba

class App extends React.Component {
  constructor() {
    super()

    this.state = {
      // ID which was just scanned
      scannedId: null,
      // text currently in the field at bottom of screen
      scanField: '',
      // current time
      time: Date.now(),
      // student looked up from DB
      student: null,
      // all classes tracked by this system
      classes: [],
<<<<<<< HEAD
      // Error/success codes generated after the student is found
      scanStatus: null,
      // matched class
      matchedClass: null,
=======
>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba
    }

    // used for focusing the scan field
    this.scanField = React.createRef()

    // interval timers which need to be cleaned up before exit
    this.intervals = []
  }

  async componentDidMount() {
    // regularly update current time
    this.intervals.push(
      asyncInterval(10000, async () => {
<<<<<<< HEAD
=======
        // Get the full class list from Populi
>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba
        this.setState({ time: Date.now() })
      }),
    )

    // watch for DB changes
    dbSub(this.handleDBChange)

    // fetch all classes
    const classes = await getAllClasses()
    this.setState({ classes })
  }

  // clean up
  componentWillUnmount() {
    // interval updates
    this.intervals.forEach((intervalId) => {
      cancelInterval(intervalId)
    })

    // results timeout
    this.clearTimeout()

    // stop watching for DB changes
    dbUnsub(this.handleDBChange)
  }

  // extend time that the results screen will be shown
  restartTimeout = () => {
    this.clearTimeout()
    this.resetTimer = window.setTimeout(this.resetScan, 30000)
<<<<<<< HEAD
  }

  // Call this fn to return to the welcome screen after displaying scan results
  resetScan = () => {
    clearTimeout()

    this.setState({ scannedId: null })
  }

=======
  }

  // Call this fn to return to the welcome screen after displaying scan results
  resetScan = () => {
    clearTimeout()

    this.setState({ scannedId: null })
  }

>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba
  // clear timeout
  clearTimeout = () => {
    if (typeof this.resetTimer !== 'undefined') {
      window.clearTimeout(this.resetTimer)
    }
  }

  // Focus the scan field when the screen is touched.
  // A user will naturally touch the screen if the device is unresponsive.
  focusInput = () => {
    this.scanField.current.focus()
  }

  // handle typing into the scan field
  handleScanField = (e) => {
    this.setState({ scanField: e.target.value, scannedId: null })
  }

  // Enter key was pressed in the ID field.
  // run the scan
  handleScanSubmit = async (e) => {
    // don't post to the server, lol
    e.preventDefault()

<<<<<<< HEAD
    // preliminary UI updates
    this.setState({ scanField: '' })
    this.restartTimeout()

    // extract id and lookup student
    const id = this.state.scanField.trim()
    const student = await studentFromId(id)

    // handle case of student not found
    if (!student) {
      this.setState({ scannedId: id, student: null })
    }

    // find if the student is attending any classes
    // covered by this scanner
    const currentClasses = this.currentClasses()
    const matchingClasses = _.intersection(
      currentClasses.map((c) => c.classInstanceId),
      student.enrolledClasses.map((c) => c.courseInstanceId),
    )

    // no match, no write
    if (matchingClasses.length === 0) {
      this.setState({
        scannedId: id,
        student: student,
        class: null,
        scanStatus: 'ERR_NO_MATCH',
      })
    }
    // matched exactly one class.  Write to DB
    else if (matchingClasses.length === 1) {
      // matchingClasses only contains IDs.  Look up the original class object.
      const currentClass = currentClasses.find(
        (c) => c.classInstanceId === matchingClasses[0],
      )

      this.setState({
        scannedId: id,
        student: student,
        matchedClass: currentClass,
        scanStatus: 'OK_MATCH_FOUND',
      })

      await writeScan(student, currentClass)
    }
    // duplicate matches, no write
    else {
      this.setState({
        scannedId: id,
        student: student,
        class: null,
        scanStatus: 'ERR_MULTIPLE_MATCHES',
      })
    }
  }

  handleDBChange = async () => {
    // re-fetch all classes
    const classes = await getAllClasses()
    this.setState({ classes })
  }

  currentClasses = () => {
    return currentClasses(this.state.classes, this.state.time)
=======
    // restart results display timeout
    this.restartTimeout()

    // extract id
    const id = this.state.scanField.trim()

    // save to state
    this.setState({ scannedId: null }, async () => {
      await this.updateStudent(id)
      this.setState({ scanField: '', scannedId: id })
    })

    // write to DB
    await writeScan(id)
  }

  // Look up student in the DB and save results to state
  // called on first scan and again whenver the DB changes
  updateStudent = async (id) => {
    if (!id) {
      return this.setState({ student: null })
    }

    const student = await studentFromId(id)
    console.log(id, student)
    this.setState({ student })
  }

  handleDBChange = async () => {
    this.updateStudent(this.state.scannedId)
>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba
  }

  render() {
    let contents = (
      <CenterText>
        <Welcome>Please scan your student ID.</Welcome>
        <InfoSubtitle
          config={this.config}
<<<<<<< HEAD
          currentClasses={this.currentClasses()}
=======
          classes={[]}
>>>>>>> dedc05faf5d9da1f3558ea84e2a9d027c869cdba
          date={this.state.time}
        />
      </CenterText>
    )
    if (this.state.scannedId) {
      contents = (
        <ScanResults
          id={this.state.scannedId}
          student={this.state.student}
          onCancel={this.resetScan}
          scanStatus={this.state.scanStatus}
          matchedClass={this.state.matchedClass}
        />
      )
    }

    const timeDisplay = dayjs(this.state.time).format('H:mm a')

    return (
      <Container onClick={this.focusInput}>
        <Time>{timeDisplay}</Time>
        {contents}
        {/* Read scans from a reader that "types" the results. */}
        {/* We use onSubmit to detect the return keypress.*/}
        <form onSubmit={this.handleScanSubmit}>
          <ScanInput
            type="text"
            value={this.state.scanField}
            autoFocus
            ref={this.scanField}
            onChange={this.handleScanField}
          />
        </form>
      </Container>
    )
  }
}

export default App

// how many minutes early may a student arrive?
const EARLY_CHECKIN = 10

// Only return classes returning now
const currentClasses = (classes, date) => {
  return classes.filter((cls) => {
    if (dayjs(cls.end).isBefore(dayjs(date))) {
      return false
    }

    if (
      dayjs(cls.start)
        .subtract(EARLY_CHECKIN, 'minute')
        .isAfter(dayjs(date))
    ) {
      return false
    }

    return true
  })
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
`

const CenterText = styled.div`
  margin: auto;
  text-align: center;
`

const Welcome = styled.h1``

const ScanInput = styled.input`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  box-sizing: border-box;
  border: none;
  outline: none;
  background: transparent;
`

const Time = styled.div`
  position: fixed;
  top: 1rem;
  left: 1rem;

  color: gray;
  font-size: 1.2rem;
`
