import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
`

const ResultsBody = styled.div`
  flex: 1;
  display: flex;
  background: ${(p) => (p.err ? '#ff00004d' : 'hsla(89, 73%, 75%, 1)')};
`

const Message = styled.div`
  font-size: 3em;
  font-weight: bold;
  margin: auto;
  text-align: center;
`

const ScanResults = ({ id, student, scanStatus, onCancel, matchedClass }) => {
  let message
  let err

  if (!student) {
    err = true
    message = (
      <Message>
        No data for:
        <br />
        {id}
      </Message>
    )
  } else {
    let name = id
    if (student.firstName || student.lastName) {
      const first = student.firstName || ''
      const last = student.lastName || ''
      name = [first, last].join(' ')
    }

    err = true
    message = (
      <Message>
        Unknown Error
        <br />
        {name}
      </Message>
    )

    if (scanStatus === 'ERR_NO_MATCH') {
      err = true
      message = (
        <Message>
          No classes found for
          <br />
          {name}
        </Message>
      )
    } else if (scanStatus === 'OK_MATCH_FOUND') {
      err = false
      message = (
        <Message>
          Welcome to {matchedClass.classAbbrev},<br />
          {name}
        </Message>
      )
    } else if (scanStatus === 'ERR_MULTIPLE_MATCHES') {
      err = true
      message = (
        <Message>
          Conflicting classes found for
          <br />
          {name}
        </Message>
      )
    }
  }

  return (
    <Container onClick={onCancel}>
      <ResultsBody err={err}>{message}</ResultsBody>
    </Container>
  )
}

export default ScanResults
