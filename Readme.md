# Attendance Scanner

Any computer running a browser and connected to the appropriate VLAN or VPN can become a scanner.  Just visit https://server_url/scan in a modern browser.  The page will be cached for offline use by a service worker and the class/student database will be cached in IndexedDB.

The Raspberry Pis are configured for auto-startup using this guide: https://blog.gordonturner.com/2017/12/10/raspberry-pi-full-screen-browser-raspbian-december-2017/.  Specifically, I added the appropriate commands in `.config/lxsession/LXDE-pi/autostart`.

The Pi runs PouchDB and syncs all scans with the master database, effectively creating distributed backups of the full attendance log all over campus.

The Pi is capable of scanning for multiple classes.  It determines which class is being scanned for based on which class the student is enrolled in.

The scanner webui was bootstrapped with create-react-app so we don't have to understand or configure a bajilion different tools. (See https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f)

## To do next time

* Prevent nginx from responding with an octet stream.  Maybe add txt extension?
* Add version display to upper-right corner of screen
